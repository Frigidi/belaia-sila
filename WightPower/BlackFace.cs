﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WightPower
{
    class BlackFace:Face
    {
        private static Random rnd = new Random();

        public BlackFace(int hp):base((char)1,hp) {}

        public void RandomPosition()
        {
            base.SetX(rnd.Next(Get_MIN_X(), Get_MAX_X()));
            base.SetY(rnd.Next(Get_MIN_Y(), Get_MAX_Y()));
        }
    }
}
