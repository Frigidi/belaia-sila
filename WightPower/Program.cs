﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WightPower
{
    class Program
    {
        static void Main(string[] args)
        {
            BlackFace bfBad = new BlackFace(-2);
            BlackFace bfGood = new BlackFace(1);

            WhiteFace wf = new WhiteFace(10);

            int countEaten = 0;

            bfBad.RandomPosition();
            bfGood.RandomPosition();

            while(wf.GetHP()>0)
            {
                Console.Clear();
                Console.WriteLine($"HP: {wf.GetHP()}");
                Console.WriteLine($"count Eaten: {countEaten}");

                bfGood.Show();
                bfBad.Show();
                wf.Show();

                wf.Move(Console.ReadKey().Key);

                if(wf.IsEqualCoords(bfGood) == true)
                {
                    countEaten++;
                    wf.EatAnotherFace(bfGood);

                    bfBad.RandomPosition();
                    bfGood.RandomPosition();
                }

                if (wf.IsEqualCoords(bfBad) == true)
                {
                    countEaten++;
                    wf.EatAnotherFace(bfBad);

                    bfBad.RandomPosition();
                    bfGood.RandomPosition();
                }
            }
            Console.SetCursorPosition(10, 10);
            Console.WriteLine("GAME OVER");
            Console.ReadKey();
        }
    }
}
