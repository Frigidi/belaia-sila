﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WightPower
{
    abstract class Face
    {
        private int _MAX_X = Console.WindowWidth - 2;
        private int _MAX_Y = Console.WindowHeight - 2;
        private int _MIN_X = 0;
        private int _MIN_Y = 2;


        private char _look;
        private int _x, _y;
        private int _hp;
        
        public Face(char look, int hp)
        {
            _x = 0;
            _y = 3;
            _look = look;
            _hp = hp;            
        }

        public void Show()
        {
            Console.SetCursorPosition(_x, _y);
            Console.Write(_look);
        }

        protected void SetX(int x)
        {
            if(x>=_MIN_X && x <= _MAX_X)
            {
                _x = x;
            }
        }

        protected void SetY(int y)
        {
            if (y >= _MIN_Y && y <= _MAX_Y)
            {
                _y = y;
            }
        }

        protected int GetX() { return _x; }
        protected int GetY() { return _y; }

        public bool IsEqualCoords(Face face)
        {
            return _x == face._x && _y == face._y;
        }

        public void EatAnotherFace(Face face)
        {
            _hp += face._hp;
        }

        public int GetHP()
        {
            return _hp;
        }

        protected int Get_MIN_X()
        {
            return _MIN_X;
        }

        protected int Get_MIN_Y()
        {
            return _MIN_Y;
        }

        protected int Get_MAX_X()
        {
            return _MAX_X;
        }

        protected int Get_MAX_Y()
        {
            return _MAX_Y;
        }
    }
}
