﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WightPower
{
    class WhiteFace : Face
    {
        public WhiteFace(int hp) : base((char)2, hp) { }

        public void Move(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    SetX(GetX() - 1);
                    break;
                case ConsoleKey.RightArrow:
                    SetX(GetX() + 1);
                    break;
                case ConsoleKey.UpArrow:
                    SetY(GetY() - 1);
                    break;
                case ConsoleKey.DownArrow:
                    SetY(GetY() + 1);
                    break;
            }
        }
    }
}
